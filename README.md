# Sample Squirrel
> _Squirrel: It's like ClickOnce but Works™_

A sample to get started with Squirrel (Econocom "meetup" presentation)

The squirrel documentation is really well done so I'm not going to repeat : see the [Squirrel documentation](https://github.com/Squirrel/Squirrel.Windows/blob/develop/docs/readme.md)

<!--
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
-->

# Step by step
## Prerequis
If you want to automate the whole process, you'll need `NuGet CLI` (Download it [here](https://docs.microsoft.com/en-us/nuget/install-nuget-client-tool))

## Installing Squirrel
Add the package [Squirrel.Window](https://www.nuget.org/packages/squirrel.windows/) to your solution.

## Create the nuspec file
There is several ways to do this and let you browse the Internet.

Create the nuspec file at the root of the solution (content example below).

```xml
<?xml version="1.0" encoding="utf-8"?>
<package xmlns="http://schemas.microsoft.com/packaging/2013/05/nuspec.xsd">
<metadata>
	<id>PACKAGE_ID</id> <!-- This is used to named the nuget package to create-->
	<version>$version$</version> <!-- This will be replaced by the assembly value -->
	<title>PROJECT_TITLE</title>
	<authors>AUTHORS</authors>
	<owners>AUTHORS</owners>
	<requireLicenseAcceptance>false</requireLicenseAcceptance>
	<description>DESCRIPTION</description>
	<copyright>COPYRIGHT</copyright>
	<dependencies />
</metadata>
<files>
    <!-- Include/Exclude all the files you need -->
    <!-- One line per targeted framework -->
	<file src="**\*" target="lib\net461" exclude="*.pdb;*.nupkg;*.vshost.*" />
</files>
</package>
```
[More informations](https://docs.microsoft.com/en-us/nuget/create-packages/creating-a-package) on nuspec file.

## Update code
Now the package is installed, we need to manage the updates. This example uses a folder that contains the updates.

```csharp
using (var mgr = new UpdateManager(@"PATH_TO_THE_FOLDER_CONTAINING_UPDATES"))
{
    SquirrelAwareApp.HandleEvents(
        onInitialInstall: _ => mgr.CreateShortcutForThisExe(),
        onAppUpdate: _ => mgr.CreateShortcutForThisExe(),
        onAppObsoleted: _ => mgr.RemoveShortcutForThisExe(),
        onAppUninstall: _ => mgr.RemoveShortcutForThisExe());
    await mgr.UpdateApp();
    RestartApplication(); // I needed this to restart the app after the update
}
```

## Automate the process
Now we need to :
- create the NuGet package which will be used
- create the executable

I decided to automate this in the `.csproj`.

1. Edit the `.csproj`
2. Check if the Squirrel package have been imported.
    ```xml
    <Import Project="..\packages\squirrel.windows.1.9.1\build\squirrel.windows.targets" Condition="Exists('..\packages\squirrel.windows.1.9.1\build\squirrel.windows.targets')" />
    ```
3. Create the new target
    * Get the `assembly file` (used to retrieve informations in it)
    * Insert an exec command to create the `nupkg file` in the build folder
    * Insert the squirrel command to create the `release files`
        - it will create a full version of the application
        - it will also create a delta version of the application
        > Note : Depending on what you want to do in your update code, you'll be able to download, apply, restart etc your application

    Example below :
    ```xml
    <Target Name="AfterBuild" Condition=" '$(Configuration)' == 'Debug'">
      <!-- Check and get the assembly file -->
	  <GetAssemblyIdentity AssemblyFiles="$(TargetPath)">
		<Output TaskParameter="Assemblies" ItemName="currentAssembly"/>
	  </GetAssemblyIdentity>
      <!-- Create the exec command to create the nupkg -->
	  <Exec Command='$(SolutionDir)packages\NuGet.CommandLine.4.9.4\tools\NuGet.exe pack $(SolutionDir)$(SolutionName).nuspec -Properties "Configuration=$(ConfigurationName);Platform=AnyCPU" -Version %(currentAssembly.Version) -OutputDirectory $(OutDir) -BasePath $(OutDir)' />
	  <Exec Command='$(SolutionDir)packages\squirrel.windows.1.9.1\tools\Squirrel.exe --releasify $(ProjectDir)$(OutDir)$(ProjectName).$([System.Version]::Parse(%(currentAssembly.Version)).ToString(3)).nupkg -releaseDir $(SolutionDir)Releases -icon $(ProjectDir)Squirrel.ico' />
	</Target>
    ```
4. Build your solution. You'll see new files created in the releaseDir path provided in the command line of Squirrel.

> Notes : The msi package won't work on the machine who build the solution ([More informations](https://github.com/Squirrel/Squirrel.Windows/blob/master/docs/using/machine-wide-installs.md))