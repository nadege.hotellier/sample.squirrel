﻿using Squirrel;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Sample.Squirrel.Demo
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override async void OnStartup(StartupEventArgs e)
        {
            SplashScreen splashScreen = new SplashScreen();
            splashScreen.Show();

            try {
                await UpdateIfAvailable();
                base.OnStartup(e);
                MessageBox.Show("update finished, show window");
                MainWindow mainWindow = new MainWindow();
                splashScreen.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async Task UpdateIfAvailable()
        {
            using (var mgr = new UpdateManager(@"C:\Users\everg\Documents\Projets\Sample.Squirrel\Releases\"))
            {
                SquirrelAwareApp.HandleEvents(
                    onInitialInstall: _ => mgr.CreateShortcutForThisExe(),
                    onAppUpdate: _ => mgr.CreateShortcutForThisExe(),
                    onAppObsoleted: _ => mgr.RemoveShortcutForThisExe(),
                    onAppUninstall: _ => mgr.RemoveShortcutForThisExe());
                await mgr.UpdateApp();
                RestartApplication();
            }
        }

        private void RestartApplication()
        {
            Application.Current.Exit += delegate (object sender, ExitEventArgs e)
            {
                System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
            };
            Application.Current.Shutdown();
        }
    }
}
