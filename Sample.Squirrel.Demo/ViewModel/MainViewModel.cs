using GalaSoft.MvvmLight;

namespace Sample.Squirrel.Demo.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel() { }

        private string _status = "hello vous";
        public string Status
        {
            get { return this._status; }
            set
            {
                this._status = value;
                Set(() => this.Status, ref this._status, value);
            }
        }

        private string _version = "1.1";
        public string Version
        {
            get { return $"Version {this._version}"; }
            set
            {
                this._version = value;
                Set(() => this.Version, ref this._version, value);
            }
        }

        private int _progress = 0;
        public int Progress
        {
            get { return this._progress; }
            set
            {
                this._progress = value;
                Set(() => this.Progress, ref this._progress, value);

                this.IsProgressVisible = value > 0;
            }
        }

        private bool _isProgressVisible = false;
        public bool IsProgressVisible
        {
            get { return this._isProgressVisible; }
            set
            {
                this._isProgressVisible = value;
                Set(() => this.IsProgressVisible, ref this._isProgressVisible, value);
            }
        }
    }
}